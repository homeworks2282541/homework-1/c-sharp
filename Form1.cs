using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace hw1
{
    public partial class Form1 : Form
    {

        public Form1()
        {
            InitializeComponent();
        }

        private void drawRectangle(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            Pen pen = new Pen(Color.Black, 5);
            
            g.DrawRectangle(pen, 10, 10, 100, 50);
            pen.Dispose();
        }

        private void drawCircle(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            Pen pen = new Pen(Color.Black, 5);

            g.DrawEllipse(pen, new Rectangle(10, 10, 50, 50));
            pen.Dispose();
        }

        private void drawLine(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            Pen pen = new Pen(Color.Black, 5);

            g.DrawLine(pen, 10, 10, 100, 50);
            pen.Dispose();
        }

        private void drawPoint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            Pen pen = new Pen(Color.Black, 10);

            g.DrawEllipse(pen, 10, 10, 10, 10);
            pen.Dispose();
        }
    }
}
